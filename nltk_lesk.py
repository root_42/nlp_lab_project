from nltk.wsd import lesk
import codecs
cricket_bat = codecs.open("cricketbat.txt", 'r', "utf-8")
cricket_bat = cricket_bat.read().lower()
mammal_bat = codecs.open("vampirebat.txt", 'r', "utf-8")
mammal_bat = mammal_bat.read().lower()
ambiguous_word = "bat"

# using lesk to predict criket bat
print(lesk(cricket_bat, ambiguous_word))
#print(lesk(cricket_bat, ambiguous_word.definition()))

# --------------------------------------------------------------------------------------
# using lesk to predict mammal bat
print(lesk(mammal_bat, ambiguous_word))
#print(lesk(mammal_bat, ambiguous_word.definition()))
